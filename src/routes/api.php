<?php

use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Route;

Route::get("/users/{id}", [UserController::class, "get"])->name("users.get");
Route::get("/users", [UserController::class, "list"])->name("users.list");
Route::post("/users", [UserController::class, "create"])->name("users.post");
Route::patch("/users/{id}", [UserController::class, "update"])->name("users.patch");
Route::delete("/users/{id}", [UserController::class, "remove"])->name("users.delete");
Route::put("/users/{id}/restore", [UserController::class, "restore"])->name("users.restore");
