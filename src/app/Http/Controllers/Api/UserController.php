<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;

class UserController extends Controller
{

    public function __construct(
        protected User $model,
    ) {
        
    }
    
    public function list(): ResourceCollection
    {
        $users = $this->model->paginate();
        return UserResource::collection($users);
    }

    public function get(int $id): UserResource
    {
        $user = $this->model->findOrFail($id);
        return new UserResource($user);
    }

    public function create(CreateUserRequest $request): UserResource
    {
        $data = $request->validated();
        $user = $this->model->create($data);

        return new UserResource($user);
    }

    public function update(UpdateUserRequest $request, int $id): UserResource
    {
        $user = $this->model->findOrFail($id);
        $data = $request->validated();

        $user->update($data);
        return new UserResource($user);
    }

    public function remove(int $id): Response
    {
        $this->model->findOrFail($id)->delete();

        //return response()->json([], Response::HTTP_NO_CONTENT);
        return response()->noContent();
    }

    public function restore(int $id)
    {
        $user = $this->model::onlyTrashed()->findOrFail($id);
        $user->restore();
        return new UserResource($user);
    }
}
