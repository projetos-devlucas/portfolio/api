FROM php:8.2-apache

COPY ./src /var/www/html/
COPY default.conf /etc/apache2/sites-enabled

RUN apt-get update \
    && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip \
    && apt-get autoremove -y \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd sockets

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf \
    && a2enmod rewrite \
    && a2enmod headers \
    && a2enmod rewrite \
    && a2dissite 000-default \
    && service apache2 restart

ENV COMPOSER_ALLOW_SUPERUSER=1
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN  composer install --no-interaction \
    && php artisan key:generate

WORKDIR /var/www/html/public

EXPOSE 80